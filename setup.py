from setuptools import setup


setup(
    setup_requires=['zetup >= 0.2.43'],

    use_zetup=True,
)